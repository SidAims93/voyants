@extends('layouts.app')

@section('content')
    <div class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{route('authors.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Name : </td>
                                    <td>
                                        <input type="text" name="name" class="form-control" required>
                                        @error('name')
                                            <span style="color:red !important">{{$message}}</span>    
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td>Email : </td>
                                    <td>
                                        <input type="email" name="email" class="form-control" required>
                                        @error('email')
                                            <span style="color:red !important">{{$message}}</span>    
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td>Badge :</td>
                                    <td>
                                        <select name="badge_id" class="form-control" required>
                                            <option value="">-- SELECT --</option>
                                            @foreach($badges as $badge)
                                                <option value="{{$badge->id}}">{{$badge->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('badge_id')
                                            <span style="color:red !important">{{$message}}</span>    
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td>Books Published :</td>
                                    <td>
                                        <select multiple name="books[]" class="form-control">
                                            @foreach ($books as $book)
                                                <option value="{{$book->id}}">{{$book->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('books')
                                            <span style="color:red !important">{{$message}}</span>    
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="submit" class="btn btn-md btn-success" value="CREATE"></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection