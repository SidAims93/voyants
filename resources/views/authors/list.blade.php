@extends('layouts.app')

@section('content')
    <div class="text-center">
        <div class="container mb-1 mt-1">
        <form action="{{route('authors.index')}}" method="GET">
            <input type="text" placeholder="Search By Any Keyword Here..." class="form-control" name="searchTerm">
            <input type="submit" class="btn btn-md btn-success">
        </form>
        </div>
        <a href="{{route('authors.create')}}" class="btn btn-md btn-success">Create New Author</a>
        @if($authors !== null && $authors->count() > 0)
            <table class="table">
                <thead>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Badge</th>
                    <th>Books Count</th>
                    <th>Books List</th>
                </thead> 
                <tbody>
                    @foreach($authors as $author)
                    <tr>
                        <td>{{$author->name}}</td>
                        <td>{{$author->email}}</td>
                        <td>{{$author->getBadge->name}}</td>
                        <td>{{$author->getBooksData->count()}}</td>
                        <td>{!! implode(', <br>',$author->getBooksData->pluck('name')->toArray()) !!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$authors->links()}}
        @else
        <div class="mt-1">
            <h5>No Authors Found !! Please create one !!</h5>
        </div>
        @endif
    </div>
@endsection