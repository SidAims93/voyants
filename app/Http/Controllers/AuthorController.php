<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAuthor;
use App\Models\Author;
use App\Models\AuthorBooks;
use App\Models\Badge;
use App\Models\Book;
use Exception;
use Illuminate\Http\Request;

class AuthorController extends Controller {

    public function __construct() {
        //$this->middleware('auth');    
    }

    public function index(Request $request) {
        $authors = $this->getAuthors($request->all());
        return view('authors.list', ['authors' => $authors]);
    }

    private function getAuthors($request) {
        try {
            return Author::with(['getBadge', 'getBooksData'])->where(function ($query) use ($request) {
                if(isset($request['searchTerm'])) 
                    return $query->where('name', 'like', '%'.$request['searchTerm'].'%')
                                ->orWhere('email', 'like', '%'.$request['searchTerm'].'%')
                                ->orWhereHas('getBooksData', function ($q) use ($request) {
                                    return $q->where('name', 'like', '%'.$request['searchTerm'].'%')
                                            ->orWhere('isbn', 'like', '%'.$request['searchTerm'].'%');
                                });
            })->paginate(5);
        } catch(Exception $e) {
            dd($e->getMessage());
            return null;
        }
    }

    public function create() {
        return view('authors.create', ['badges' => Badge::get(), 'books' => Book::get()]);
    }

    public function store(CreateAuthor $request) {
        try {
            $request = $request->validated();
            $author = Author::updateOrCreate(['email' => $request['email']], [
                'email' => $request['email'],
                'name' => $request['name'],
                'badge_id' => $request['badge_id'] 
            ]);
            foreach($request['books'] as $book_id) //Because of lack of time i did this .. i would converted into one single Raw SQL query otherwise
                AuthorBooks::updateOrCreate(['author_id' => $author->id, 'book_id' => $book_id], ['author_id' => $author->id, 'book_id' => $book_id]);
            return redirect()->route('authors.index');
        } catch(Exception $e) {
            dd($e->getMessage().' '.$e->getLine());
        }
    }
}
