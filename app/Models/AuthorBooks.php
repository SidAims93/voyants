<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuthorBooks extends Model
{
    protected $guarded = [];

    public function getAuthorDetails() {
        return $this->belongsTo(Author::class, 'author_id');
    }
}
