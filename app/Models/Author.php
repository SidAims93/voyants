<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model {

    protected $guarded = [];
    
    public function getBadge() {
        return $this->hasOne(Badge::class, 'id', 'badge_id');
    }

    public function getBooksData() {
        return $this->hasManyThrough(Book::class, AuthorBooks::class,  'author_id', 'id', 'id', 'book_id')->orderBy('books.avg_rating', 'desc');
    }

    public function getBooksMapping() {
        return $this->hasMany(AuthorBooks::class, 'author_id');
    }
}
