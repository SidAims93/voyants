<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder {
    /**
     * Run the database seeds.
     * @return void
     */
    public function run() {
        $default_user = 'admin@voyants.com';
        $default_fields = [
            'name' => 'Default User',
            'email' => $default_user,
            'password' => Hash::make('123456'),
            'email_verified_at' => date('Y-m-d h:i:s')
        ];
        User::updateOrCreate(['email' => $default_user], $default_fields);
    }
}
