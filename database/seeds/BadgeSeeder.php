<?php

use App\Models\Badge;
use Illuminate\Database\Seeder;

class BadgeSeeder extends Seeder {
    /**
     * Run the database seeds.
     * @return void
     */
    public function run() {
        $badge_arr = [
            'Platinum' => 'Platinum Description',
            'Gold' => 'Gold Description', 
            'Silver' => 'Silver Description',
            'Bronze' => 'Bronze Description'
        ];
        foreach($badge_arr as $name => $desc) 
            Badge::updateOrCreate(['name' => $name, 'description' => $desc]);
    }
}
