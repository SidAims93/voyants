<?php

use App\Models\Book;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder {
    /**
     * Run the database seeds.
     * @return void
     */
    public function run() {
        $books_arr = [
            ['name' => 'Harry Potter', 'price' => 500, 'isbn' => 'demo123', 'avg_rating' => 4.7, 'publish_date' => '2004-04-10'],
            ['name' => 'The Girl with the Dragon Tattoo', 'price' => 1200, 'isbn' => 'demo1234', 'avg_rating' => 5.2, 'publish_date' => '2005-03-10'],
            ['name' => 'The Monk with the Ferrari', 'price' => 1300, 'isbn' => 'abx123', 'avg_rating' => 3.9, 'publish_date' => '2006-02-10'],
            ['name' => 'James Bond 007', 'price' => 2000, 'isbn' => 'qwerty123', 'avg_rating' => '1234', 'publish_date' => '2010-03-01'],
            ['name' => 'Dragon Ball Z', 'price' => 2500, 'isbn' => 'poiuyt123', 'avg_rating' => 1.4, 'publish_date' => '2020-03-10']
        ];
        foreach($books_arr as $data)
            Book::updateOrCreate(['name' => $data['name']], $data);
    }
}
