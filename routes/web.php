<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//All routes are disabled for authentication but if it wasnt then i would have done it this way

//Route::middleware('auth')->group(function () {
    Route::get('/', 'AuthorController@index');
    Route::resource('authors', 'AuthorController');
    Route::resource('books', 'BooksController');
//});
Route::get('home', 'HomeController@index')->name('home');
